import React, { Component } from 'react';
import Message from './Message';
import TrouveNom from './TrouveNom';
import axios from 'axios';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      message: "",
      nom: "",
      email: "",
      id: ""
    }
  }

  componentDidMount(){

          axios.get("http://localhost:8080/api/handlemessage")
          .then(response => {

            this.setState({
              message:response.data.message
            })
            console.log(this.state);
          })

          .catch((error) => {
            console.log("error",error)
          })
      }

  render() {
    return (
      <div >
        <Message message={this.state.message}></Message>
        <TrouveNom trouveNom={this.trouveNom.bind(this)}/>
        <p>Nom: {this.state.nom}</p>
        <p>Email: {this.state.email}</p>
        <p>Id: {this.state.id}</p>
      </div>
    );
  }

trouveNom(nom) {
  axios.get("http://localhost:8080/api/trouvepersonne/" + nom)
  .then(response => {
    this.setState({
      nom:response.data.nom,
      email: response.data.email,
      id: response.data.id
    });
    console.log(this.state);
  })
  .catch((error) => {
    console.log("error",error)
  })

  }
}

export default App;
